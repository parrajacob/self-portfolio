import React, { Component } from "react";
import "./App.css";
import TypedReact from "./TypedReact";

class App extends Component {
  render() {
    return (
      <div className="App">
      <h1 className="name">Jacob Parra</h1>
      <h3 className="name">Software Developer</h3>
      <br></br>
        <TypedReact
          strings={[
            '$ jacobisraelparra@gmail.com^1000\n$ 602-123-4567^1000\n$ www.gitlab.com/parrajacob^1000\n$ www.github.com/parrajacob^1000\n$ www.linkedin.com/in/jacob-parra-dev'
          ]}
        />
      </div>
    );
  }
}

export default App;
